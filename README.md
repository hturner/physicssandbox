# Physics Sanbox

This sandbox was created for the authors Final Year Project at the University Of Leeds in 2021.

## Run

A `.jar` executable has been created for ease of use.
This can be downloaded and ran without the need to compile the source code.

## Dependencies

The Processing library is required in order to build the source code.

This can be downloaded [here](https://processing.org/download/).

The `core.jar` file then needs to be added to the libraries of the project.
On Windows and Linux, this file is located in the Processing distribution folder inside a folder named lib. On Mac OS X, right-click the Processing.app and use "Show Package Contents" to see the guts. The core.jar file is inside Contents → Resources → Java.
This should be added to a `lib/` folder in the source code.

More information on how to add the `core.jar` file to the class path can be found [here](https://github.com/processing/processing-library-template#AddingJARs)

## Build

If you would prefer to compile the source code directly.
A release can be downloaded from this repository containing the source code.

Once the `core.jar` file has been added to a `lib/` folder the following commands can be run to build the application, or simply the `compile.bash` script.

```bash
> javac src/*.java -d classes -cp .\lib\core.jar
> java -cp classes:lib/core.jar physicssandbox

```
