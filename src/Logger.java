import java.io.FileWriter; 
import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter; 
import java.io.File; 
import java.io.IOException; 

public class Logger {
    String outputFilename;
    String headerNames = "optimisation, noOfParticles, collisions, checks, energy, frames, maxDepth\n";
    physicssandbox p;

    public Logger(String outputFilename, physicssandbox p) { 
        this.p = p;
        try {
            File file = new File(System.getProperty("user.dir"), outputFilename);
            file.createNewFile();
        } catch (IOException e){
            e.printStackTrace();
        }
        this.outputFilename = String.format("%s%s", System.getProperty("user.dir"), outputFilename);
        System.out.println(this.outputFilename);
        this.printHeader();
    }

    public void logData() {
        try {
            FileWriter writer = new FileWriter(outputFilename, true);
            String record = String.format("%s, %d, %d, %d, %f, %d, %d\n", getOptimisationType(), p.NUMBER_OF_PARTICLES, p.sandbox.getCollisions(), p.sandbox.getChecks(), p.sandbox.getEnergy(), p.sandbox.getFrames(), p.sandbox.maxDepth);
            writer.write(record);
            writer.close();
            p.timer.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getOptimisationType() {
        if (p.useQuadTreeCheck.getValue()) return "quadtree";
        else if(p.useVerletListCheck.getValue()) return "verlet";
        else if(p.useVerletQuadTreeCheck.getValue()) return "verletQuadtree";
        else return "none";
    }

    public void printHeader() {
        try {
            FileWriter writer = new FileWriter(outputFilename, true);
            LocalDateTime datetime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            String formattedDate = datetime.format(formatter);
            writer.write(String.format("%s\n", formattedDate));
            writer.write(headerNames);
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
