import java.util.ArrayList;

import processing.core.PVector; 

public class VerletList {
    private int cutoffRadius;
    private ArrayList<Particle> particles;
    public Particle mainParticle;
    Circle circle;
    physicssandbox p;
    PVector originalPosition;

    public VerletList(Particle particle, int radius, physicssandbox p) {
        this.originalPosition = new PVector(particle.position.x, particle.position.y);
        this.p = p;
        this.mainParticle = particle;
        this.particles = new ArrayList<Particle>();
        this.cutoffRadius = radius;
        this.circle = new Circle(particle.position.x, particle.position.y, radius, p);
    }

    public boolean isDisplacementGreaterThanThreshold() {
        return PVector.dist(originalPosition, mainParticle.position) >= p.VERLET_THRESHOLD;
    }

    public int getParticlesSize() {
        return particles.size();
    }

    public Particle getParticle(int index) {
        return particles.get(index);
    }

    public void render() {
        if (this.mainParticle.showList) {
            p.fill(255);
            p.text(this.particles.size(), mainParticle.position.x, mainParticle.position.y-6);
        }
        this.circle.render();
    }

    public boolean containsParticle(Particle particle) {
        if (particle == mainParticle) return false;
        return this.circle.containsParticle(particle);
    }

    public void insertParticle(Particle particle) {
        // if (mainParticle.showList) particle.isInsideList = true;
        this.particles.add(particle);
    }

    public void resetList() {
        this.particles = new ArrayList<Particle>();
        this.circle = new Circle(this.mainParticle.position.x, this.mainParticle.position.y, cutoffRadius, p);
        this.originalPosition = new PVector(mainParticle.position.x, mainParticle.position.y);
    }
}
