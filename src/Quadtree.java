import java.util.ArrayList; 

public class Quadtree {
    public Rectangle boundary;
    public float capacity;
    public ArrayList<Particle> particles;
    public boolean divided;

    public Quadtree northEast, northWest, southEast, southWest;
    public ArrayList<Quadtree> boundaries;
    physicssandbox p;

    public Quadtree(Rectangle boundary, float capacity, physicssandbox p) {
        this.boundary = boundary;
        this.capacity = capacity;
        this.particles = new ArrayList<Particle>();
        this.divided = false;
        this.boundaries = new ArrayList<Quadtree>();
        this.p = p;
    }
    
    public Quadtree(Rectangle boundary, float capacity, ArrayList<Quadtree> boundaries, physicssandbox p) {
        this.boundary = boundary;
        this.capacity = capacity;
        this.particles = new ArrayList<Particle>();
        this.divided = false;
        this.boundaries = boundaries;
        this.p = p;
    }

    public boolean isInside(Particle particle) {
        return this.boundary.contains(particle);
    }

    public boolean insert(Particle particle) {
        if (!isInside(particle)) return false;
        boolean hasCapacity = this.particles.size() < this.capacity;
        if (hasCapacity) {
            this.particles.add(particle);
            return true;
        }
        if (!this.divided) divide();

        // TODO: Improve this line
        boolean isSuccessfulInsertion = this.northEast.insert(particle) ||
        this.northWest.insert(particle) ||
        this.southEast.insert(particle) ||
        this.southWest.insert(particle);
        return isSuccessfulInsertion;
    }

    private void divide() {
        createDivisions();
        // moveParticlesIntoDivisions();
        this.divided = true;
    }

    private void createDivisions() {
        float x = this.boundary.x;
        float y = this.boundary.y;
        float w = this.boundary.width/2;
        float h = this.boundary.height/2;

        Rectangle northEastRectangle = new Rectangle(x+w, y-h, w, h, p);
        Rectangle northWestRectangle = new Rectangle(x-w, y-h, w, h, p);
        Rectangle southEastRectangle = new Rectangle(x+w, y+h, w, h, p);
        Rectangle southWestRectangle = new Rectangle(x-w, y+h, w, h, p);
        this.northEast = new Quadtree(northEastRectangle, this.capacity, p);
        this.northWest = new Quadtree(northWestRectangle, this.capacity, p);
        this.southEast = new Quadtree(southEastRectangle, this.capacity, p);
        this.southWest = new Quadtree(southWestRectangle, this.capacity, p);
    }

    private void moveParticlesIntoDivisions() {
        for (int i = 0; i < this.particles.size(); i++) {
            Particle p = this.particles.get(i);
            this.northEast.insert(p);
            this.northWest.insert(p);
            this.southEast.insert(p);
            this.southWest.insert(p);
        }
        this.particles.clear();
    }

    public ArrayList<Particle> query(Circle range, ArrayList<Particle> found) {
        if (!range.intersects(this.boundary)) return found;

        if (this.divided) {
            found = this.northEast.query(range, found);
            found = this.northWest.query(range, found);
            found = this.southEast.query(range, found);
            found = this.southWest.query(range, found);
        } 
        for (int i = 0; i < this.particles.size(); i++) {
            Particle p = this.particles.get(i);
            found.add(p);
        }
        
        return found;
    }

    public void draw() {
        if (this.divided) {
            this.northEast.draw();
            this.northWest.draw();
            this.southEast.draw();
            this.southWest.draw();
        } else {
            this.boundary.draw(this.particles.size());
        }
    }

    public int getDepth() {
        if (!this.divided) return 1;
        int northEastDepth = northEast.getDepth();
        int northWestDepth = northWest.getDepth();
        int southEastDepth = northEast.getDepth();
        int southWestDepth = northWest.getDepth();

        int max = northEastDepth;
        if (northWestDepth > max)
            max = northWestDepth;
        if (southEastDepth > max)
            max = southEastDepth;
        if (southWestDepth > max)
            max = southWestDepth;
        return 1 + max;
    }
}
