import processing.core.*;
import java.util.ArrayList; 

public class Particle {
    public PVector position;
    public boolean isOverlapping;
    public float radius;
    public PVector velocity;
    private boolean hasCollided;
    public VerletList verletList;

    public int id; // for debug

    physicssandbox p;

    boolean showList;
    boolean isInsideList;

    public Particle(float x, float y, boolean shouldShowVerletList, physicssandbox p) {
        this(x, y, physicssandbox.floor(p.random(p.MIN_RADIUS, p.MAX_RADIUS)), shouldShowVerletList, p);
    }

    public Particle(float x, float y, boolean shouldShowVerletList, physicssandbox p, int id) {
        this(x, y, physicssandbox.floor(p.random(p.MIN_RADIUS, p.MAX_RADIUS)), shouldShowVerletList, p, id);
    }

    public Particle(float x, float y, float radius, boolean shouldShowVerletList, physicssandbox p) {
        this.p = p;
        this.position = new PVector(x, y);
        this.radius = radius;
        this.isOverlapping = false;
        this.hasCollided = false;
        this.velocity = new PVector(p.random(-50, 50), p.random(-50, 50));
        this.showList = shouldShowVerletList;
    }

    public Particle(float x, float y, float radius, boolean shouldShowVerletList, physicssandbox p, int id) {
        this.p = p;
        this.position = new PVector(x, y);
        this.radius = radius;
        this.isOverlapping = false;
        this.hasCollided = false;
        this.velocity = new PVector(p.random(-50, 50), p.random(-50, 50));
        this.showList = shouldShowVerletList;
        this.id = id;
    }
    
    public float getMass() { return radius; }

    public void resetProperties() {
        setIsOverlapping(false);
        resetCollidedParticles();
    }

    public void updatePosition() {
        position.add(PVector.mult(velocity, 1.0f/p.frameRate));
    }

    private void updateVelocityAfterCollision(Particle otherParticle) { 
        PVector newVelocity = calculateNewVelocity(this, otherParticle);
        PVector newVelocityOtherParticle = calculateNewVelocity(otherParticle, this);
        
        velocity = newVelocity;
        otherParticle.velocity = newVelocityOtherParticle;
     }
     
    private PVector calculateNewVelocity(Particle A, Particle B) {
       float massConstant = (2*B.getMass()) / (A.getMass() + B.getMass());
       PVector positionSub = PVector.sub(A.position, B.position);
       float dotProduct = PVector.dot(PVector.sub(A.velocity, B.velocity), positionSub);
       float magnitude = positionSub.magSq();
       float coefficient = massConstant * (dotProduct/magnitude);
       PVector bigBoy = positionSub.mult(coefficient);
       PVector newVelocity = PVector.sub(A.velocity, bigBoy);
       return newVelocity;
     }

    private void setIsOverlapping(boolean value) {
        isOverlapping = value;
    }

    public void checkParticleOverlaps(Particle otherParticle) {
        if (this == otherParticle) return;
        p.sandbox.incrementChecks(); 
        boolean pointsAreOverlapping = isOverlapping(otherParticle);
        if (pointsAreOverlapping) setParticleIsOverlapping(otherParticle);
    }

    private boolean isOverlapping(Particle otherParticle) {
        float distanceBetweenPoints = PVector.dist(position, otherParticle.position);
        float allowedRange = (radius / 2) + (otherParticle.radius / 2);
        boolean pointsAreOverlapping = distanceBetweenPoints < allowedRange;

        return pointsAreOverlapping;
    }

    private void setParticleIsOverlapping(Particle otherParticle) {
        p.sandbox.incrementCollisions();
        setIsOverlapping(true);
        otherParticle.setIsOverlapping(true);
        if (hasCollided) return;
        updateVelocityAfterCollision(otherParticle);
        reduceStickyness(otherParticle);
        addParticlesToCollided(otherParticle);
    }

    //TODO: Improve this function
    private void reduceStickyness(Particle otherParticle) {
        float dx = this.position.x - otherParticle.position.x;
        float dy = this.position.y - otherParticle.position.y; 

        float tangent = physicssandbox.atan2(dy, dx);
        float angle = physicssandbox.HALF_PI + tangent;

        position.x += physicssandbox.sin(angle);
        position.y -= physicssandbox.cos(angle);
        otherParticle.position.x -= physicssandbox.sin(angle);
        otherParticle.position.y += physicssandbox.cos(angle);
    }

    private void addParticlesToCollided(Particle otherParticle) {
        hasCollided = true;
        otherParticle.hasCollided = true;
    }

    private void resetCollidedParticles() {
        hasCollided = false;
    }

    public void checkCollidingWithBoundary() {
        checkCollidingWithXBoundary();
        checkCollidingWithYBoundary();
    }

    private void checkCollidingWithYBoundary() {
        if (isCollidingWithTopBoundary()) {
            setIsOverlapping(true);
	    reverseYAngle();
		} else if (isCollidingWithBottomBoundary()) {
			setIsOverlapping(true);
			reverseYAngle();
		}
    }

    private void checkCollidingWithXBoundary() {
        if (isCollidingWithLeftBoundary()) {
            setIsOverlapping(true);
            reverseXAngle();
        } else if (isCollidingWithRightBoundary()) {
            setIsOverlapping(true);
            reverseXAngle();
        }
    }

    private void reverseXAngle() {velocity.x = -velocity.x; }

    private void reverseYAngle() {velocity.y = -velocity.y; }

    private boolean isCollidingWithTopBoundary() {
        return position.y + velocity.y < 0;
    }
    private boolean isCollidingWithBottomBoundary() {
        return position.y + velocity.y > p.height;
    }
    private boolean isCollidingWithLeftBoundary() {
        return position.x + velocity.x < 0;
    }
    private boolean isCollidingWithRightBoundary() {
        return position.x + velocity.x > p.width - p.INTERFACE_WIDTH;
    }

    public void initialiseVerletList() {
        this.verletList = new VerletList(this, p.VERLET_CUTOFF_RADIUS, p);
    }

    public void updateVerletList(ArrayList<Particle> particles) {
        if (!verletList.isDisplacementGreaterThanThreshold()) return;
        verletList.resetList();
        for (int i = 0; i < p.NUMBER_OF_PARTICLES; i++) {
            Particle otherParticle = particles.get(i);
            if (!verletList.containsParticle(otherParticle)) continue;
            verletList.insertParticle(otherParticle);
        }
    }

    public void render() {
        p.noStroke();
        boolean isVerletParticle = p.showVerletListCheck.getValue() && showList && (p.useVerletListCheck.getValue() || p.useVerletQuadTreeCheck.getValue());
        if (isVerletParticle) renderVerlet();
        else renderNormalParticle();
    }

    private void renderVerlet() {
        p.fill(255, 0, 0);
        p.ellipse(position.x, position.y, radius, radius);
        this.verletList.render(); 
    }

    private void renderNormalParticle() {
        boolean isInVerletList = isInsideList && p.showVerletListCheck.getValue() && (p.useVerletListCheck.getValue() || p.useVerletQuadTreeCheck.getValue());
        if (isInVerletList)
            p.fill(0, 0, 255);
        else if (isOverlapping)
            p.fill(100, 255, 0, 200);
        else
            p.fill(110);
        p.ellipse(position.x, position.y, radius, radius);
        // p.text(id, position.x, position.y); // DEBUG
    }
}
