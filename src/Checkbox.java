class Checkbox {
    int x, y;
    boolean value;
    int length = 40;
    int boxOffset = 25;
    physicssandbox p;

    Checkbox(int x, int y, boolean initial, physicssandbox p){
        this.x = x - length;
        this.y = y - length;
        this.value = initial;
        this.p = p;
    }

    public boolean getValue() {
        return value;
    }

    public void render(){
        p.stroke(255);
        p.fill(isMouseWithinBox()?128:64);
        p.rect(x, y-boxOffset, length, length, 8);
        if(value){
            p.line(x, y-boxOffset, x+length, y+length-boxOffset);
            p.line(x, y+length-boxOffset, x+length, y-boxOffset);
        }
    }

    public boolean click(){
        if(isMouseWithinBox()){
            toggleValue();
            return true;
        }
        return false;
    }

    public void toggleValue() {
        value=!value;
    }

    public void setFalse() {
        value = false;
    }

    public boolean isMouseWithinBox(){
        boolean isMouseWithinBox = p.mouseX>x&&p.mouseX<x+length&&p.mouseY>y-boxOffset&&p.mouseY<y+length-boxOffset;
        return isMouseWithinBox;
    }
}