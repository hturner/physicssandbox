import processing.core.*; 
public class Circle {
    public PVector position;
    public float radius;
    public float radiusSquared;
    physicssandbox p;

    public Circle(float x, float y, float r, physicssandbox p) {
        this.position = new PVector(x, y);
        this.radius = r;
        this.radiusSquared = r*r;
        this.p = p;
    }

    public boolean containsParticle(Particle particle) {
        float distance = physicssandbox.pow((particle.position.x - this.position.x), 2) + physicssandbox.pow((particle.position.y - this.position.y), 2);
        return distance <= this.radiusSquared;
    }

    public boolean intersects(Rectangle range) {
        float xDistance = physicssandbox.abs(range.x - this.position.x);
        float yDistance = physicssandbox.abs(range.y - this.position.y);

        float edges = physicssandbox.pow(xDistance - range.width, 2) + physicssandbox.pow(yDistance - range.height, 2);

        boolean noIntersection = xDistance > (this.radius + range.width) || yDistance > ( this.radius + range.height);
        if (noIntersection) return false;

        boolean intersectionWithinCircle = xDistance <= range.width || yDistance <= range.height;
        if (intersectionWithinCircle) return true;

        boolean intersectionOnEdgeCircle = edges <= this.radiusSquared;
        return intersectionOnEdgeCircle;
    }

    public void render() {
        p.stroke(255);
        p.noFill();
        p.ellipse(this.position.x, this.position.y, this.radius*2, this.radius*2);
    }
}