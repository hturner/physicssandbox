/** 
   Author : Dinoswarleafs (twitter.com/Dinoswarleafs) 
   Simple input on a slider like a TrackBar on Visual Studio
*/
import processing.core.*; 
import java.util.ArrayList; 
public class Slider {
 
    ArrayList<PVector> indexes = new ArrayList<PVector>();
    PVector startPos, endPos, sideValues, sCircle, cSize;
    float increment, range, indexRange, gapLength, tickHeight, selectedValue;
    boolean isActivated;
    physicssandbox p;
 
    Slider(float startX, float startY, float xLength, float startValue, float endValue, float increment_, physicssandbox p) {
      this.p = p;
      startPos       = new PVector(startX, startY);
      endPos         = new PVector(startX + xLength, startY);
      sideValues     = new PVector(startValue, endValue);
      sCircle        = new PVector(startX, startY);
      cSize          = new PVector(20, 20);
      increment      = increment_;
      range          = endPos.x - startPos.x;
      if (sideValues.x == 0 || sideValues.y == 0)
        indexRange    = (physicssandbox.abs(sideValues.x - sideValues.y) + 1) / increment;
      else
        indexRange    = physicssandbox.abs(sideValues.x - sideValues.y) / increment; 
      gapLength      = range / indexRange;
      tickHeight     = 15;
      selectedValue  = sideValues.x; 
 
      float value = sideValues.x;
      float i = startPos.x;
      for (; i < endPos.x; i += gapLength) {
        indexes.add(new PVector(value, i));
        value += increment;
      }
      indexes.add(new PVector(sideValues.y, i));
    }
 
	void update() {
		int closestIndex = closestIndex();
		selectedValue = indexes.get(closestIndex).x;
		p.updateParticles(selectedValue);
		if (p.mousePressed && clickedCircle()) 
			isActivated = true;
		if (isActivated && p.mousePressed)
			if (startPos.x < p.mouseX && p.mouseX < endPos.x)
				sCircle.x = p.mouseX;
		else {
			isActivated = false;
			sCircle.x = indexes.get(closestIndex).y;
		}
	}

 
    void display() {
        p.stroke(200);
        p.strokeWeight(2);
        p.fill(0);
        p.line(startPos.x, startPos.y, endPos.x, endPos.y);
        p.textAlign(physicssandbox.CENTER);
        for (int i = 0; i < indexes.size(); i++) {
            p.line(indexes.get(i).y, startPos.y + tickHeight, indexes.get(i).y, startPos.y - tickHeight);
        }
        p.text((int) indexes.get(0).x, indexes.get(0).y, startPos.y + (tickHeight * 2));
        p.text((int) indexes.get(indexes.size() - 1).x, indexes.get(indexes.size() - 1).y, startPos.y + (tickHeight * 2));
        p.fill(0);
        p.noStroke();
        p.ellipse(sCircle.x, sCircle.y, cSize.x, cSize.y);
    }
 
 
  boolean overButton() {
    // Checks if the mouse is within the circle
    // Source : https://processing.org/examples/button.html
    float disX = sCircle.x - p.mouseX; 
    float disY = sCircle.y - p.mouseY; 
    return (physicssandbox.sqrt(physicssandbox.sq(disX) + physicssandbox.sq(disY))) < cSize.x/2;
  }
 
    boolean clickedCircle() {
      return overButton() && p.mousePressed; 
    }
  
  int closestIndex() {
   for (int i = 0; i < indexes.size(); i++)
    if (sCircle.x < startPos.x)
     return 0;
    else if (sCircle.x > endPos.x)
     return indexes.size() - 1;
    else if (physicssandbox.abs(sCircle.x - indexes.get(i).y) < gapLength / 2) {
     return i;
    }
    physicssandbox.println("No closest index! This shouldn't be possible unless you modified & broke the code.");
    return -1;
  }
}