public class UserInterface {
    private int INTERFACE_WIDTH;
    physicssandbox p;

    public UserInterface(int INTERFACE_WIDTH, physicssandbox p) {
        this.INTERFACE_WIDTH = INTERFACE_WIDTH;
        this.p = p;
    }

    public void drawUI() {
        drawSidebar();
        printDataToScreen();
    }

    private void drawSidebar() {
        p.fill(255);
        p.rect(p.width-INTERFACE_WIDTH, 0, INTERFACE_WIDTH, p.height);
    }

    private void printDataToScreen() {
        printLoggingData();
        printCheckboxes();
        printScrollbar();
    }
    
    private void printLoggingData() {
        printCurrentFramerate();
        calculateTotalEnergyOfSystem();
        printNumberOfCollisions();
        printNumberOfChecks();
        printNumberOfParticles();
    }

    private void printScrollbar() {
        p.particleSlider.update();
        p.particleSlider.display();
    }

    private void printCheckboxes() {
        printUseQuadTreeCheckbox();
        // printUseVerletQuadTreeCheckbox();
        printUseVerletListCheckbox();
        if (p.useQuadTreeCheck.getValue() || p.useVerletQuadTreeCheck.getValue())
            printShowQuadTreeCheckbox();
        if (p.useVerletListCheck.getValue() || p.useVerletQuadTreeCheck.getValue())
            printShowVerletListCheckbox();
    }

    private void printUseQuadTreeCheckbox() {
        printTextToScreen("QuadTree: ", p.useQuadTreeCheck.y);
        p.useQuadTreeCheck.render();
    }

    private void printUseVerletQuadTreeCheckbox() {
        printTextToScreen("Verlet QuadTree: ", p.useVerletQuadTreeCheck.y);
        p.useVerletQuadTreeCheck.render();
    }

    private void printUseVerletListCheckbox() {
        printTextToScreen("Verlet List: ", p.useVerletListCheck.y);
        p.useVerletListCheck.render();
    }

    private void printShowQuadTreeCheckbox() {
        printTextToScreen("Show QuadTree: ", p.showQuadTreeCheck.y);
        p.showQuadTreeCheck.render();
    }

    private void printShowVerletListCheckbox() {
        printTextToScreen("Show Verlet List: ", p.showVerletListCheck.y);
        p.showVerletListCheck.render();
    }

    private void printCurrentFramerate() {
        printTextToScreen("FPS: ", p.sandbox.getFrames(), 50);
    }

    private void calculateTotalEnergyOfSystem() {
        printTextToScreen("Total energy: ", (int)p.sandbox.getEnergy(), 100);
    }

    private void printNumberOfCollisions() {
        printTextToScreen("No. of collisions: ", p.sandbox.getCollisions(), 150);
    }

    private void printNumberOfChecks() {
        printTextToScreen("No. of checks: ", p.sandbox.getChecks(), 200);
    }

    private void printNumberOfParticles() {
        printTextToScreen("No. of particles: ", p.sandbox.getParticlesSize(), 250);
    }

    public void printTextToScreen(String text, int value, int yPos) {
        p.textAlign(physicssandbox.LEFT);
        p.textSize(22);
        p.fill(0, 0, 0);
        p.text(text, p.width - INTERFACE_WIDTH, yPos);
        p.text(String.valueOf(value), p.width - (INTERFACE_WIDTH * 1/4), yPos);
    }

    public void printTextToScreen(String text, int yPos) {
        p.textAlign(physicssandbox.LEFT);
        p.textSize(22);
        p.fill(0, 0, 0);
        p.text(text, p.width - INTERFACE_WIDTH, yPos);
    }
}
