import processing.core.*; 

public class physicssandbox extends PApplet {
    public int INTERFACE_WIDTH = 300;
    public int CANVAS_WIDTH;

    public Checkbox useQuadTreeCheck, showQuadTreeCheck, useVerletQuadTreeCheck, useVerletListCheck, showVerletListCheck;
    public Slider particleSlider;

    public int NUMBER_OF_PARTICLES;

    public int MAX_RADIUS = 14;
    public int MIN_RADIUS = 6;

    public int VERLET_CUTOFF_RADIUS = 64;
    public int VERLET_THRESHOLD = 16;
    public int QUADTREE_CAPACITY = 8;
    
    public Sandbox sandbox;
    public Logger logger;
    public UserInterface userInterface;
    public Timer timer;

    public void setup() {
        frameRate(144);
        initialiseUI();
        initialiseObjects();
    }

    private void initialiseUI() {
        useQuadTreeCheck = new Checkbox(width - (INTERFACE_WIDTH * 1/4) , 400, false, this);
        useVerletListCheck = new Checkbox(width - (INTERFACE_WIDTH * 1/4) , 450, false, this);
        useVerletQuadTreeCheck = new Checkbox(width - (INTERFACE_WIDTH * 1/4) , 500, false, this);
        showVerletListCheck = new Checkbox(width - (INTERFACE_WIDTH * 1/4) , 550, false, this);
        showQuadTreeCheck = new Checkbox(width - (INTERFACE_WIDTH * 1/4) , 600, false, this);
        particleSlider = new Slider(width - INTERFACE_WIDTH+25, 650, INTERFACE_WIDTH - 50, 100, 4000, 100, this);
        CANVAS_WIDTH = width - INTERFACE_WIDTH;
        NUMBER_OF_PARTICLES = (int)particleSlider.selectedValue;
    }

    private void initialiseObjects() {
        logger = new Logger("/outputData.csv", this);
        userInterface = new UserInterface(INTERFACE_WIDTH, this);
        sandbox = new Sandbox(NUMBER_OF_PARTICLES, this);
        timer = new Timer();
        timer.start();
    }

    public void draw() {
        background(0);
        sandbox.mainLoop();
        userInterface.drawUI();
        boolean hasBeenOneSecond = timer.getElapsed() > 1000;
        if (hasBeenOneSecond) logger.logData();
        sandbox.resetValues();
    }

    public void updateParticles(float numberOfParticles) {
        NUMBER_OF_PARTICLES = (int)numberOfParticles;
        if (NUMBER_OF_PARTICLES < sandbox.getParticlesSize()) removeParticles();
        else if (NUMBER_OF_PARTICLES > sandbox.getParticlesSize()) addParticles();
    }

    private void addParticles() {
        while (NUMBER_OF_PARTICLES > sandbox.getParticlesSize()) {
            sandbox.createParticle();
        }
    }

    private void removeParticles() {
        while (NUMBER_OF_PARTICLES < sandbox.getParticlesSize()) {
            sandbox.removeParticle();
        }
    }

    public void mouseClicked() {
        clickRadioButtons();
        clickShowButtons();
    }

    public void clickRadioButtons() {
        if (useQuadTreeCheck.click()) {
            useVerletQuadTreeCheck.setFalse();
            useVerletListCheck.setFalse();
        } else if (useVerletQuadTreeCheck.click()) {
            useQuadTreeCheck.setFalse();
            useVerletListCheck.setFalse();
        } else if (useVerletListCheck.click()) {
            useVerletQuadTreeCheck.setFalse();
            useQuadTreeCheck.setFalse();
        }
    }

    public void clickShowButtons() {
        boolean quadtreeIsActive = useQuadTreeCheck.getValue() || useVerletQuadTreeCheck.getValue();
        boolean verletListIsActive = useVerletListCheck.getValue() || useVerletQuadTreeCheck.getValue();
        if (quadtreeIsActive) {
            showQuadTreeCheck.click();
        }
        if (verletListIsActive) {
            showVerletListCheck.click();
        }
    }

    public void settings() {  size(1280, 720); }

    static public void main(String[] passedArgs) {
        String[] appletArgs = new String[] { "physicssandbox" };
        if (passedArgs != null) {
            PApplet.main(concat(appletArgs, passedArgs));
        } else {
            PApplet.main(appletArgs);
        }
    }
}
