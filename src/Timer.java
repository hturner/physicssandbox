public class Timer {
    long startTime;
    long stopTime;
    boolean finished;

    public Timer() { }

    public void start() {
        this.startTime = System.currentTimeMillis();
    }

    public void stop() {
        this.stopTime = System.currentTimeMillis();
        this.finished = true;
    }

    public long getElapsed() {
        return this.finished ? this.stopTime - this.startTime : System.currentTimeMillis() - this.startTime;
    }


    public void reset() {
        this.startTime = System.currentTimeMillis();
        this.stopTime = 0;
        this.finished = false;
    }
}
