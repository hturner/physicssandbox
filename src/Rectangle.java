public class Rectangle {
    float x, y;
    float width, height;
    physicssandbox p;

    public Rectangle(float x, float y, float w, float h, physicssandbox p) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
        this.p = p;
    }

    public boolean contains(Particle particle) {
        boolean withinUpperBound = particle.position.y <= this.y + this.height;
        boolean withinLowerBound = particle.position.y >= this.y - this.height;
        boolean withinRightBound = particle.position.x <= this.x + this.width;
        boolean withinLeftBound = particle.position.x >= this.x - this.width;
        return withinUpperBound && withinLowerBound && withinRightBound && withinLeftBound;
    }

    public boolean intersects(Rectangle range) {
        return !(range.x - range.width > this.x + this.width ||
        range.x + range.width < this.x - this.width ||
        range.y - range.height > this.y + this.height ||
        range.y + range.height < this.y - this.height);
    }

    public void draw() {
        p.stroke(255);
        p.noFill();
        p.strokeWeight(1);
        p.rectMode(physicssandbox.CENTER);
        p.rect(this.x, this.y, this.width*2, this.height*2);
        p.rectMode(physicssandbox.CORNER);
    }

    public void draw(int num) {
        p.stroke(255, 100);
        p.noFill();
        p.strokeWeight(1);
        p.rectMode(physicssandbox.CENTER);
        p.rect(this.x, this.y, this.width*2, this.height*2);
        p.fill(255);
        p.text(num, this.x, this.y);
        p.rectMode(physicssandbox.CORNER);
    }
}
