import java.util.ArrayList; 

public class Sandbox {
    int numberOfCollisions;
    int numberOfChecks;
    int numberInVerletList;
    int maxDepth;
    float totalEnergy;
    ArrayList<Particle> particles;
    physicssandbox p;

    public Sandbox(int NUMBER_OF_PARTICLES, physicssandbox p) {
        this.p = p;
        this.numberOfChecks = 0;
        this.numberOfCollisions = 0;
        this.totalEnergy = 0;
        this.numberInVerletList = 0;
        this.maxDepth = 0;
        this.particles = new ArrayList<Particle>();
        for (int particleIndex = 0; particleIndex < NUMBER_OF_PARTICLES; particleIndex++) {
            boolean shouldShowVerletList = particleIndex % 10 == 0;
            // createParticle(shouldShowVerletList);
            createParticle(shouldShowVerletList, particleIndex); // DEBUG
        }
    }

    public void createParticle() {
        Particle particle = new Particle(p.random(p.width-p.INTERFACE_WIDTH-10), p.random(p.height), false, p);
        this.particles.add(particle);
        particle.initialiseVerletList();
    }

    public void createParticle(boolean shouldShowVerletList) {
        Particle particle = new Particle(p.random(p.width-p.INTERFACE_WIDTH-10), p.random(p.height), shouldShowVerletList, p);
        this.particles.add(particle);
        particle.initialiseVerletList();
    }

    // for debug
    public void createParticle(boolean shouldShowVerletList, int id) {
        Particle particle = new Particle(p.random(p.width-p.INTERFACE_WIDTH-10), p.random(p.height), shouldShowVerletList, p, id);
        this.particles.add(particle);
        particle.initialiseVerletList();
    }

    public void removeParticle() {
        this.particles.remove(p.NUMBER_OF_PARTICLES-1);
    }

    public int getParticlesSize() {
        return particles.size();
    }

    public void setCollisions(int numberOfCollisions) { this.numberOfCollisions = numberOfCollisions; }
    public int getCollisions() { return this.numberOfCollisions; }
    public void incrementCollisions() { this.numberOfCollisions++; }

    public void setChecks(int numberOfChecks) { this.numberOfChecks = numberOfChecks; }
    public int getChecks() { return this.numberOfChecks; }
    public void incrementChecks() { this.numberOfChecks++; }

    public int getFrames() { return physicssandbox.floor(p.frameRate); }

    public void setEnergy(float totalEnergy) { this.totalEnergy = totalEnergy; }
    public float getEnergy() { return calculateEnergy(); }
    public float calculateEnergy() {
        float totalEnergy = 0;
        for (int particleIndex = 0; particleIndex < particles.size(); particleIndex++) {
            Particle currentParticle = particles.get(particleIndex);
            float particleEnergy = calculateEnergyOfParticle(currentParticle);
            totalEnergy += particleEnergy;
        }
        return totalEnergy;
    }

    private float calculateEnergyOfParticle(Particle particle) {
        float particleEnergy = 0.5f * particle.radius * particle.velocity.magSq();
        return particleEnergy;
    }

    public void resetValues() {
        this.numberOfChecks = 0;
        this.numberOfCollisions = 0;
        this.totalEnergy = 0;
        this.numberInVerletList = 0;
        this.maxDepth = 0;
    }

    public void mainLoop() {
        Quadtree qtree = initialiseQuadtree();
        if ((p.useVerletListCheck.getValue() || p.useVerletQuadTreeCheck.getValue())) updateVerletLists(); 
        if (p.showQuadTreeCheck.getValue() && (p.useQuadTreeCheck.getValue() || p.useVerletQuadTreeCheck.getValue())) qtree.draw();
        particleLoop(qtree);
    }

    public void particleLoop(Quadtree qtree) {
        for (int particleIndex = 0; particleIndex < particles.size(); particleIndex++) {
            Particle currentPoint = particles.get(particleIndex);
            pointUpdates(qtree, currentPoint);
        }
        for (int particleIndex = 0; particleIndex < particles.size(); particleIndex++) {
            Particle currentPoint = particles.get(particleIndex);
            pointActions(currentPoint);
        }
    }

    public Quadtree initialiseQuadtree() {
        if (!p.useQuadTreeCheck.getValue() && !p.useVerletQuadTreeCheck.getValue()) return null;
        Rectangle boundary = new Rectangle(p.CANVAS_WIDTH/2, p.height/2, p.CANVAS_WIDTH/2, p.height/2, p);
        Quadtree qtree = new Quadtree(boundary, p.QUADTREE_CAPACITY, p);

        for (int i = 0; i < p.NUMBER_OF_PARTICLES; i++) {
            Particle particle = particles.get(i);
            qtree.insert(particle);
        }
        this.maxDepth = qtree.getDepth();
        return qtree;
    }

    public void initialiseVerletLists() {
        for (int i = 0; i < p.NUMBER_OF_PARTICLES; i++) {
            Particle particle = particles.get(i);
            particle.initialiseVerletList();
        }
    }

    public void updateVerletLists() {
        for (int i = 0; i < p.NUMBER_OF_PARTICLES; i++) {
            Particle particle = particles.get(i);
            particle.updateVerletList(particles);
        }
    }

    public void pointUpdates(Quadtree qtree, Particle currentPoint) {
        if (p.useVerletQuadTreeCheck.getValue()) {
            checkOtherPoints(currentPoint);
        } else if (p.useQuadTreeCheck.getValue()) {
            checkOtherPointsWithQuadtree(qtree, currentPoint);
        } else if(p.useVerletListCheck.getValue()) {
            checkOtherPointsWithVerletList(currentPoint); 
        } else {
            checkOtherPoints(currentPoint);
        }
        currentPoint.checkCollidingWithBoundary();
        currentPoint.updatePosition();
    }

    public void pointActions(Particle currentPoint) {
        currentPoint.render();
        currentPoint.resetProperties();
    }

    public void checkOtherPointsWithQuadtree(Quadtree qtree, Particle currentPoint) {
        Circle range = new Circle(currentPoint.position.x, currentPoint.position.y, p.VERLET_CUTOFF_RADIUS, p);
        ArrayList<Particle> otherParticles = qtree.query(range, new ArrayList<Particle>());
        for (int particleIndex = 0; particleIndex < otherParticles.size(); particleIndex++) {
            Particle otherPoint = otherParticles.get(particleIndex);
            currentPoint.checkParticleOverlaps(otherPoint);
        }
    }

    public void checkOtherPointsWithVerletList(Particle currentPoint) {
        VerletList verlet = currentPoint.verletList;
        int numberOfParticlesInList = verlet.getParticlesSize();
        numberInVerletList += numberOfParticlesInList;
        for (int particleIndex = 0; particleIndex < numberOfParticlesInList; particleIndex++) {
            Particle otherPoint = verlet.getParticle(particleIndex);
            currentPoint.checkParticleOverlaps(otherPoint);
        }
    }

    public void checkOtherPoints(Particle currentPoint) {
        for (int particleIndex = 0; particleIndex < p.NUMBER_OF_PARTICLES; particleIndex++) {
            Particle otherPoint = particles.get(particleIndex);
            currentPoint.checkParticleOverlaps(otherPoint);
        }
    }
}
